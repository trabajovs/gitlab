## Importación de bibliotecas

El script comienza importando las bibliotecas necesarias: pandas y matplotlib.pyplot.

## Carga de datos

El script carga los datos del archivo CSV 'SensorData.csv' utilizando la función `read_csv` de pandas.

## Obtención de datos para la gráfica

El script obtiene los datos específicos para la gráfica, que son la temperatura y la humedad, de las columnas 'temperatureSHT31' y 'humiditySHT31' del DataFrame.

## Creación de la gráfica

El script crea una gráfica utilizando matplotlib. La gráfica tiene un tamaño de 8x6 y muestra dos líneas: una para la temperatura (en rojo) y otra para la humedad (en azul). Las etiquetas de los ejes x e y son 'Muestras' y 'Valor', respectivamente. El título de la gráfica es 'Gráfico de Temperatura y Humedad'. La gráfica también incluye una leyenda y una cuadrícula.

## Guardado de la gráfica

El script guarda la gráfica como una imagen .png con el nombre 'temperatura_humedad.png' utilizando la función `savefig` de matplotlib.