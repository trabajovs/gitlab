import pandas as pd
import matplotlib.pyplot as plt

# Cargar el archivo CSV
data = pd.read_csv('./SensorData.csv')

# Obtener datos específicos para la gráfica
temperatura = data['temperatureSHT31']
humedad = data['humiditySHT31']

# Crear la gráfica
plt.figure(figsize=(8, 6))  # Tamaño de la figura
plt.plot(temperatura, label='Temperatura SHT31', color='red')  # Gráfico de temperatura
plt.plot(humedad, label='Humedad SHT31', color='blue')  # Gráfico de humedad
plt.xlabel('Muestras')  # Etiqueta del eje x
plt.ylabel('Valor')  # Etiqueta del eje y
plt.title('Gráfico de Temperatura y Humedad')  # Título de la gráfica
plt.legend()  # Mostrar leyenda
plt.grid(True)  # Mostrar cuadrícula
plt.tight_layout()  # Ajustar diseño
plt.savefig('./temperatura_humedad.png')  # Guardar gráfico como imagen .png
plt.show()  # Mostrar la gráfica