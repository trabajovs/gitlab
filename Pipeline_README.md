## Etapas del Pipeline

El pipeline se divide en tres etapas principales:

1. **Build**: En esta etapa, se clona el repositorio y se ejecuta un script de Python.
2. **Upload**: En esta etapa, se suben las gráficas generadas a Figshare.
3. **Documentation**: En esta etapa, se genera documentación a partir de un archivo README.md y se incrustan las gráficas en la documentación.

## Variables

Las variables utilizadas en el pipeline son:

- `SCRIPT_PATH`: Ruta al script de Python que se va a ejecutar.
- `DATA_PATH`: Ruta al archivo de datos CSV.
- `FIGSHARE_API_TOKEN`: Token de la API de Figshare para autenticación.

## Tareas

Cada etapa del pipeline consta de una o más tareas que se ejecutan en orden. Las tareas son:

- `tasks`: Clona el repositorio, cambia al directorio del repositorio y ejecuta el script de Python.
- `upload_to_figshare`: Sube las gráficas generadas a Figshare.
- `generate_documentation`: Genera documentación a partir de un archivo README.md e incrusta las gráficas en la documentación.

## Tags

Las tareas se etiquetan con `python`, lo que significa que se ejecutan en un runner que tiene la etiqueta `python`.

## Antes del script

Antes de ejecutar el script, se actualiza el sistema y se instalan Python y la biblioteca matplotlib.

## Documentación

La documentación se genera a partir de un archivo README.md utilizando `pandoc`. Las gráficas se incrustan en la documentación utilizando una etiqueta de imagen HTML.
